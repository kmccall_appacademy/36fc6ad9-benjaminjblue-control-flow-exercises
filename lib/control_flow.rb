# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
    str.delete(('a'..'z').to_a.join)
end


# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
    half_len = (str.length + 1) / 2 - 1
    str[(half_len)...-(half_len)]
end


# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
    str.count('aeiouAEIOU')
end


# Return the factorial of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
    (1..num).reduce(1, :*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
    str = arr[0].to_s
    arr.drop(1).each do |seg|
        str += separator + seg.to_s
    end
    str
end


# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
    weirdString = ""
    str.each_char.with_index{|ch, i|
        weirdString += (i % 2 == 0 ? ch.downcase : ch.upcase)
    }
    weirdString
end



# Reverse all words of five or more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
    str.split.map{|word|
        word.length > 4 ? word.reverse : word
    }.join(' ')
end


# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
    arr = (1..n).to_a
    i = 2
    while i <= n
        arr[i] = 'fizz'
        i += 3
    end

    i = 4
    while i <= n
        arr[i] = 'buzz'
        i += 15
    end

    i = 9
    while i <= n
        arr[i] = 'buzz'
        i += 15
    end

    i = 14
    while i <= n
        arr[i] = 'fizzbuzz'
        i += 15
    end

    arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
    arr.slice(0..-1).reverse
end


# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
    return false if num == 1
    i = 2
    while i <= num / 2
        return false if num % i == 0
        i += 1
    end
    true
end


# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
    i = 2
    facts = [1]
    while i <= num / 2
        facts << i if num % i == 0
        i += 1
    end
    facts.push(num)
    facts
end


# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
    factors(num).select{|x| prime?(x)}
end


# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
    prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
    is_even = arr[0,3].select{|x| x % 2 == 0}.length > 1
    i = 0
    while i < arr.length
        return arr[i] if is_even == (arr[i] % 2 != 0)
        i += 1
    end
end

